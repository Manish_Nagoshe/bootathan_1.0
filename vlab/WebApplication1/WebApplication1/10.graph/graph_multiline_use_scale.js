﻿function graphline(divid,datapoints,xtitle,ytitle,max11,min11) {

    var chart = new CanvasJS.Chart(divid, {
        zoomEnabled: true,

        toolTip: {
            shared: false

        },
        legend: {
            verticalAlign: "top",
            horizontalAlign: "center",
            fontSize: 14,
            fontWeight: "bold",
            fontFamily: "calibri",
            fontColor: "dimGrey"
        },
        axisY: {
            title: ytitle,
            includeZero: false,
	    minimum:min11,
	    maximum:max11	
        },
        axisX: {
            title: xtitle,
          minimum:min11,
	      maximum:max11
        },

        data: datapoints
    })
    chart.render();

}
